package id.carsworld.mvvmpattern.data.repository

import id.carsworld.mvvmpattern.data.db.QuoteDao
import id.carsworld.mvvmpattern.data.model.Quote

class QuoteRespository(private val quoteDao: QuoteDao): View_QuoteRepository {
    override fun setQuote(quote: Quote) {
        quoteDao.setQuote(quote)
    }

    override fun getQuotes() = quoteDao.getQuotes()

}