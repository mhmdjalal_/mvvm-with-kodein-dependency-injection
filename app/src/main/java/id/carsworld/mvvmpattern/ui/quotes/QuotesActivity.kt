package id.carsworld.mvvmpattern.ui.quotes

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import id.carsworld.mvvmpattern.R
import id.carsworld.mvvmpattern.data.model.Quote
import kotlinx.android.synthetic.main.activity_quotes.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein
import org.kodein.di.generic.instance

class QuotesActivity : AppCompatActivity(), KodeinAware {
    override val kodein: Kodein by closestKodein()
    private val viewModelFactory: QuotesViewModelFactory by instance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quotes)
        initializeUI()
    }

    fun initializeUI() {
        /*Use ViewModelProviders class to create/get already created QuoteViewModel
        * for this view (activity)
        */
        val viewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(QuotesViewModel::class.java)

        /*Observing livedata from the QuotesViewModel which in turn observes
        * LiveData from the repository, which observes LiveData from the DAO
        * */
        viewModel.getQuote().observe(this, Observer {
            val stringBuilder = StringBuilder()
            it.forEach {
                stringBuilder.append("$it\n\n")
            }
            textView_quotes.text = stringBuilder.toString()
        })

        /* When the button is clicked, instantiate a Quote and add it to DB throught the ViewModel
        * */
        button_add_quote.setOnClickListener {
            val quote = Quote(
                editText_quote.text.toString(),
                editText_author.text.toString()
            )
            viewModel.addQuote(quote)
            editText_author.setText("")
            editText_quote.setText("")
        }
    }
}
