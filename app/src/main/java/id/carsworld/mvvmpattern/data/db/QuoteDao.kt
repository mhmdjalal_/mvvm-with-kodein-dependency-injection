package id.carsworld.mvvmpattern.data.db

import androidx.lifecycle.LiveData
import id.carsworld.mvvmpattern.data.model.Quote

interface QuoteDao {
    fun setQuote(quote: Quote)
    fun getQuotes(): LiveData<List<Quote>>
}