package id.carsworld.mvvmpattern.ui.quotes

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import id.carsworld.mvvmpattern.data.repository.View_QuoteRepository

class QuotesViewModelFactory (private val viewQuoteRepository: View_QuoteRepository): ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return QuotesViewModel(viewQuoteRepository) as T
    }
}