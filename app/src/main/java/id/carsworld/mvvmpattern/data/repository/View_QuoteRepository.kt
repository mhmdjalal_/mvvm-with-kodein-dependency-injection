package id.carsworld.mvvmpattern.data.repository

import androidx.lifecycle.LiveData
import id.carsworld.mvvmpattern.data.model.Quote

interface View_QuoteRepository {
    fun setQuote(quote: Quote)
    fun getQuotes(): LiveData<List<Quote>>
}