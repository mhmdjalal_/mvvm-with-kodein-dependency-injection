package id.carsworld.mvvmpattern

import android.app.Application
import id.carsworld.mvvmpattern.data.db.Database
import id.carsworld.mvvmpattern.data.db.FakeDatabase
import id.carsworld.mvvmpattern.data.db.QuoteDao
import id.carsworld.mvvmpattern.data.repository.View_QuoteRepository
import id.carsworld.mvvmpattern.data.repository.QuoteRespository
import id.carsworld.mvvmpattern.ui.quotes.QuotesViewModelFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class QuotesAppplication: Application(), KodeinAware {
    override val kodein: Kodein = Kodein.lazy {
        bind<Database>() with singleton { FakeDatabase() }
        bind<QuoteDao>() with singleton { instance<Database>().quoteDao }
        bind<View_QuoteRepository>() with singleton { QuoteRespository(instance()) }
        bind() from provider { QuotesViewModelFactory(instance()) }
    }
}