package id.carsworld.mvvmpattern.ui.quotes

import androidx.lifecycle.ViewModel
import id.carsworld.mvvmpattern.data.model.Quote
import id.carsworld.mvvmpattern.data.repository.View_QuoteRepository

class QuotesViewModel (private val viewQuoteRepository: View_QuoteRepository) : ViewModel() {

    fun getQuote() = viewQuoteRepository.getQuotes()

    fun addQuote(quote: Quote) = viewQuoteRepository.setQuote(quote)

}