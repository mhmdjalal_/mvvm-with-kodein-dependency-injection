package id.carsworld.mvvmpattern.data.db

interface Database {
    val quoteDao: QuoteDao
}